export const Terms = [
  {
    term: "Backend Development",
    definitions:
      "“Backend”, in this case, refers to a layer of the technology stack. This mostly involves writing, or maintaining, a web service with a database for keeping data.Often times, backend development is associated with technologies such as Java, Scala, Python, DynamoDB, MongoDB, nodeJS, MySQL, MapReduce and many, many more. This is in contrast with front-end development, which often refers to the user interface (UI) layer, or customer-facing side of a feature.",
    examples: [],
    sources: [
      {
        source: "freecodecamp",
        url:
          "https://www.freecodecamp.org/news/these-are-the-top-computer-science-terms-you-should-know-if-youre-new-to-programming-fc8592242622/"
      }
    ]
  },
  {
    term: "Programming language",
    definitions: `A computer is very good at executing the commands it is told. In order for us human beings to communicate with a computer, we need a common set of understandable languages —a set of instructions that both humans and computers can agree on and communicate with. In the human world, we use languages such as English, Mandarin, Spanish etc to communicate. When it comes to computers, we call it a programming language. A programming language allows humans and computers to communicate. Examples of programming languages are Java, C++, Python and many more.`,
    examples: ["JavaScript", "PHP", "JAVA", "Ruby", "Python"],
    sources: [
      {
        source: "freecodecamp",
        url:
          "https://www.freecodecamp.org/news/these-are-the-top-computer-science-terms-you-should-know-if-youre-new-to-programming-fc8592242622/"
      }
    ]
  },
  {
    term: "Algorithms",
    definitions: `A process or set of rules to be followed in calculations or other problem-solving operations, especially by a computer. — Google :) Computers are great at executing instructions, and as software engineers we write instructions for the computers to execute. An algorithm is a list of steps or instructions that can be performed with or without a computer in order to solve a specific problem. One of the first problems we learn as computer science students is the problem of sorting. The essence of it is to sort a set or a collection of items in the most efficient manner.There are many ways of sorting. A popular one is Bubble Sort, otherwise known as “comparison sort”. It involves comparing and picking the larger of two items and repeating for all other elements until everything is eventually sorted. Others include Insertion Sort, Merge Sort and many, many more! I highly encourage you to explore them, as it’s a fascinating introduction to algorithms. Here’s a Youtube video to learn more about sorting algorithms.`,
    examples: [],
    sources: [
      {
        source: "freecodecamp",
        url:
          "https://www.freecodecamp.org/news/these-are-the-top-computer-science-terms-you-should-know-if-youre-new-to-programming-fc8592242622/"
      }
    ]
  },
  {
    term: "API",
    definitions: `API, a.k.a application programming interface, a.k.a a source of information. In more concrete terms, imagine you had a lemonade well at home. Your neighbors want a sip of your lemonade well, but you don’t want the well to get polluted. What do you do, then?.You decided to build a hose that connects to a small hose outside your house. All your neighbors who want a taste of your magical lemonade will line up and open the hose to access your lemonade. All this without ever coming close to your well, and you can also control when to pump the lemonade, swap out the lemonade if your well runs dry someday, or maybe charge people who drink a lot of lemonade. This is the idea behind many companies these days — these companies build a “well” of precious data and then sell it to others who wish to access that data via an API.`,
    examples: [],
    sources: [
      {
        source: "freecodecamp",
        url:
          "https://www.freecodecamp.org/news/these-are-the-top-computer-science-terms-you-should-know-if-youre-new-to-programming-fc8592242622/"
      }
    ]
  },
  {
    term: "BUG",
    definitions: `'
                    It is not a bug it's a feature'.You might’ve seen the following meme on social media websites, and might not have understood what it really meant.A bug is an error in a computer program that prevents the program from running as expected. For example, let’s assume you downloaded an iOS application, which is a type of computer program, that is supposed to show the local weather temperature in Fahrenheit. However, the iOS application might show the temperature in Celsius only, instead of the Fahrenheit you expected. In such a case, the program is not performing as expected (should show Fahrenheit, not Celsius), and you would call the error a bug. This is so common in the industry that software engineers turned it into an inside joke, hence the meme, “it’s not a bug, it’s a feature”. `,
    examples: [],
    sources: [
      {
        source: "freecodecamp",
        url:
          "https://www.freecodecamp.org/news/these-are-the-top-computer-science-terms-you-should-know-if-youre-new-to-programming-fc8592242622/"
      }
    ]
  }
];
