import { Terms } from "./list-terms";
const ul_element = document.getElementById("list-wrapper"); //get the element that will conttain our list
const query_input = document.getElementById("filtertForm"); //get the element from where we will make search

//We create a function that will be used to filter up element from a search query
const searchQuery = (terms, query) => {
  return terms.filter(term => {
    return term.term.toLowerCase().indexOf(query.toLowerCase()) !== -1;
  });
};

//We create a function that will list elements
const listingTerms = Terms => {
  let list_elements = "";
  let search_value = "";
  Terms.map(term => {
    list_elements += `<li><h1>${term.term}</h1><p>${term.definitions}</p></li>`;
  });

  ul_element.innerHTML = JSON.parse(JSON.stringify(list_elements));
};

listingTerms(Terms);

//listen the input form and filter up on keyup
query_input.addEventListener("keyup", event => {
  let search_value = query_input.value.trim();
  let Filter = searchQuery(Terms, search_value);
  listingTerms(Filter);
});
